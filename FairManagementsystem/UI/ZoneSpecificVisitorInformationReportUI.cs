﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FairManagementsystem.BLL;
using FairManagementsystem.DAL.DAO;

namespace FairManagementsystem.UI
{
    public partial class ZoneSpecificVisitorInformationReportUI : Form
    {
        public ZoneSpecificVisitorInformationReportUI()
        {
            InitializeComponent();
            LoadAllZone();
            ZoneType zoneName = (ZoneType)zoneTypeComboBox.SelectedItem;
            LoadZoneTypeVisitor(zoneName);
            totalTextBox.Text = Convert.ToString(visitorDetails.Count);

        }

        private int totalVisitorNumber;
        ZoneTypeManager zoneManager=new ZoneTypeManager();
        VisitorManager visitorManager =new VisitorManager();
        private List<int> visitorIdList; 
        private List<Visitor> visitorDetails;
        public void LoadAllZone()
        {
            zoneTypeComboBox.DataSource = zoneManager.ZoneList();
            zoneTypeComboBox.DisplayMember = "ZoneType";
            zoneTypeComboBox.ValueMember = "ID";

        }

        public void LoadZoneTypeVisitor(ZoneType zoneName)
        {
          visitorDetails= new List<Visitor>();

          visitorIdList = zoneManager.ZoneTypeVisitor(zoneName);
            foreach (int id in visitorIdList)
            {
                Visitor aVisitor=new Visitor();
                aVisitor = visitorManager.GetVisitorInfo(id);
                visitorDetails.Add(aVisitor);
            }
           
            LoadDataGridView(visitorDetails);

        }

        public void LoadDataGridView(List<Visitor> visitorList)
        {
            zoneTypeVisitorsDataGridView.Rows.Clear();

            foreach (Visitor visitor in visitorList)
            {
                zoneTypeVisitorsDataGridView.Rows.Add(visitor.Name, visitor.Email, visitor.ContactNo);
            }
        }

        private void showButton_Click(object sender, EventArgs e)
        {
            ZoneType zoneName = (ZoneType)zoneTypeComboBox.SelectedItem;
            LoadZoneTypeVisitor(zoneName);
            totalTextBox.Text = Convert.ToString(visitorDetails.Count);
        }

        
        

        

       
    }
}
